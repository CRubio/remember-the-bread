import flask
import json

from . import create_response, handle_errors
from src.business import tasks


tasks_crud = flask.Blueprint('tasks', __name__)


@handle_errors
@tasks_crud.route('/', methods=['GET'])
def list():

    """ List the tasks collection """

    entities = tasks.list()
    return create_response(entities)


@handle_errors
@tasks_crud.route('/', methods=['POST'])
def create():

    """ Create the given task """

    data = json.loads(flask.request.data)
    entities = tasks.create(data)
    return create_response(entities, 201)


@handle_errors
@tasks_crud.route('/<task_id>', methods=['DELETE'])
def delete(task_id):

    """ Delete the given task """

    tasks.delete(task_id)
    return create_response(None, 204)
